# python36-requests

This repo contains the RPM spec file to enable creating binary RPMs for `python36-requests`.

## Note for packagers: 

This `Makefile` builds a source `rpm` for `python36-requests`, but must be build with 
the `--without tests` option enabled. Otherwise, `rpmbuild` will look for 
`python36-pytest-mock`, which isn't available in our repos. 


## Updating this repo to the latest template

```
python -m cookiecutter https://git.ligo.org/packaging/rhel/spec-repo-template --overwrite-if-exists --output-dir .. --no-input
```
