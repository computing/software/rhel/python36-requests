PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

all: sources srpm

sources: $(PACKAGE).spec
	spectool -g $(PACKAGE).spec
	cp patches/* .

srpm:
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs --without tests $(PACKAGE).spec

clean:
	rm requests-v2.20.0.tar.gz
	ls patches/ | xargs rm -f
	rm -f $(PACKAGE)-*.src.rpm
